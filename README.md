# bcd #

*bcd* is some kind of  „cd with bookmarks“ for Bourne shell compatible shells like Bash or Zsh.

You define a bookmark for named `some-bookmark` by using

    bcdadd some-bookmark
	
Then you can use

    bcd some-bookmark

to cd to the directory associated with some-bookmark.


To remove a bookmark use:

    bcddel some-bookmark

Or just edit `~/.bcdrc` with your favourite editor.

Format of `~/.bcdrc` is:

name:directory

e.g.:

    ul:/usr/local
    uls:/usr/local/stow
    nspool:/var/spool/news

With that you can use:

    $ bcd uls

to cd to `/usr/local/stow`.

Calling `bcd` without arguments prints the bookmarks:

    $ bcd
           ul --> /usr/local
          uls --> /usr/local/stow
       nspool --> /var/spool/news

`bcd` and `bcddel` come with completion for Zsh and Bash:
 
 Zsh:
 
    $ bcd ul<TAB>
    Completing bcd bookmarks
           ul --> /usr/local
          uls --> /usr/local/stow


Bash:

    $ bcd ul<TAB>
    ul   uls

`bcd` works with any Bourne shell compatible shell if you remove the completion stuff at the end of the file.
